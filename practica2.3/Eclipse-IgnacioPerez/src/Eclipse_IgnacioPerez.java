import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Toolkit;
import java.awt.Font;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JToolBar;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.BorderLayout;
import javax.swing.JTextPane;
import javax.swing.Box;
import java.awt.Component;
import javax.swing.JSeparator;
import java.awt.Scrollbar;
import javax.swing.JSpinner;
import javax.swing.ButtonGroup;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JSlider;

/**  
 * @author napeiba92
 * @since 11/01/2018
 */

public class Eclipse_IgnacioPerez {

	private JFrame frmLogintravel;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eclipse_IgnacioPerez window = new Eclipse_IgnacioPerez();
					window.frmLogintravel.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Eclipse_IgnacioPerez() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogintravel = new JFrame();
		frmLogintravel.setFont(new Font("Modern No. 20", Font.PLAIN, 12));
		frmLogintravel.setTitle("LoginTravel");
		frmLogintravel.setIconImage(Toolkit.getDefaultToolkit().getImage(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/airplane-flight-in-circle-around-earth (3).png")));
		frmLogintravel.setBounds(100, 100, 383, 372);
		frmLogintravel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogintravel.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 620, 336);
		frmLogintravel.getContentPane().add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Transporte", new ImageIcon(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/airplane-around-earth (1).png")), panel_1, null);
		panel_1.setLayout(null);
		
		JRadioButton rdbtnIdaYVuelta = new JRadioButton("Ida y vuelta");
		rdbtnIdaYVuelta.setBackground(SystemColor.inactiveCaption);
		buttonGroup.add(rdbtnIdaYVuelta);
		rdbtnIdaYVuelta.setBounds(6, 7, 92, 23);
		panel_1.add(rdbtnIdaYVuelta);
		
		JRadioButton rdbtnSoloIda = new JRadioButton("Solo ida");
		rdbtnSoloIda.setBackground(SystemColor.inactiveCaption);
		buttonGroup.add(rdbtnSoloIda);
		rdbtnSoloIda.setBounds(114, 7, 73, 23);
		panel_1.add(rdbtnSoloIda);
		
		JRadioButton rdbtnMltiplesTrayectos = new JRadioButton("M\u00FAltiples trayectos");
		rdbtnMltiplesTrayectos.setBackground(SystemColor.inactiveCaption);
		buttonGroup.add(rdbtnMltiplesTrayectos);
		rdbtnMltiplesTrayectos.setBounds(201, 7, 140, 23);
		panel_1.add(rdbtnMltiplesTrayectos);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(6, 37, 354, 7);
		panel_1.add(separator);
		
		JLabel lblDesde = new JLabel("  Desde");
		lblDesde.setBounds(10, 148, 48, 23);
		panel_1.add(lblDesde);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Madrid", "Barcelona", "C\u00E1diz ", "Huelva"}));
		comboBox.setToolTipText("Madrid\r\nBarcelona");
		comboBox.setBounds(56, 148, 110, 23);
		panel_1.add(comboBox);
		
		JLabel lblA = new JLabel(" A");
		lblA.setBounds(201, 148, 22, 23);
		panel_1.add(lblA);
		
		JCheckBox chckbxTren = new JCheckBox("Avi\u00F3n");
		buttonGroup_1.add(chckbxTren);
		chckbxTren.setBounds(6, 46, 63, 23);
		panel_1.add(chckbxTren);
		
		JCheckBox checkBox = new JCheckBox("Tren");
		buttonGroup_1.add(checkBox);
		checkBox.setBounds(70, 46, 52, 23);
		panel_1.add(checkBox);
		
		JCheckBox chckbxBarco = new JCheckBox("Barco");
		buttonGroup_1.add(chckbxBarco);
		chckbxBarco.setBounds(135, 46, 67, 23);
		panel_1.add(chckbxBarco);
		
		JCheckBox chckbxCocheAlquiler = new JCheckBox("Coche alquiler");
		buttonGroup_1.add(chckbxCocheAlquiler);
		chckbxCocheAlquiler.setBounds(211, 46, 110, 23);
		panel_1.add(chckbxCocheAlquiler);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(6, 72, 354, 7);
		panel_1.add(separator_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/statue-of-liberty (1).png")));
		lblNewLabel.setBounds(0, 76, 92, 72);
		panel_1.add(lblNewLabel);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setToolTipText("Madrid\r\nBarcelona");
		comboBox_1.setBounds(229, 148, 118, 23);
		panel_1.add(comboBox_1);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/taj-mahal (3).png")));
		label.setBounds(219, 84, 102, 64);
		panel_1.add(label);
		
		JLabel lblDel = new JLabel("    Del");
		lblDel.setBounds(6, 184, 36, 23);
		panel_1.add(lblDel);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1515366000000L), null, null, Calendar.DAY_OF_YEAR));
		spinner.setBounds(56, 182, 110, 23);
		panel_1.add(spinner);
		
		JLabel lblAl = new JLabel(" Al");
		lblAl.setBounds(201, 182, 22, 23);
		panel_1.add(lblAl);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1515366000000L), null, null, Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(228, 182, 118, 23);
		panel_1.add(spinner_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(6, 220, 354, 7);
		panel_1.add(separator_2);
		
		JSlider slider = new JSlider();
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setMinimum(1);
		slider.setMaximum(5);
		slider.setMajorTickSpacing(1);
		slider.setBounds(150, 238, 200, 14);
		panel_1.add(slider);
		
		JLabel lblNPersonas = new JLabel("   n\u00BA personas");
		lblNPersonas.setBounds(10, 229, 88, 30);
		panel_1.add(lblNPersonas);
		
		JLabel lblEquipaje = new JLabel("    Equipaje");
		lblEquipaje.setBounds(10, 265, 88, 30);
		panel_1.add(lblEquipaje);
		
		JSlider slider_1 = new JSlider();
		slider_1.setPaintTicks(true);
		slider_1.setPaintLabels(true);
		slider_1.setMinimum(1);
		slider_1.setMaximum(10);
		slider_1.setMajorTickSpacing(1);
		slider_1.setBounds(150, 270, 200, 14);
		panel_1.add(slider_1);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Usuario", new ImageIcon(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/user (1).png")), panel, null);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Alojamiento", new ImageIcon(Eclipse_IgnacioPerez.class.getResource("/imagenes_Eclipse/bed.png")), panel_2, null);
		panel_2.setLayout(null);
	}
}
