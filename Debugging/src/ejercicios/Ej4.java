package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			
			/* El error se produce debido a que el divisor por el que va a ser
			 * dividido el numero introducido puede llegar hasta cero ya que, en el bucle,
			 * el valor i puede ser igual a cero. Una divisi�n entre un n�mero y cero es imposible. 
			 */
			
			/* Un posible solucion seria sustituir el comando 'i>=0' que aparece en el bucle por
			 * 'i>0'. De esta forma, el divisor nunca llega a cero. 
			 */
			
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}