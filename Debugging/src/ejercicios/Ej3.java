package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=27;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		/*Si la cadena no contiene el caracter se�alado -como es el caso-, al
		 * int posicionCaracter se le asigna el valor -1. Cuando ese valor pasa 
		 * al substring que queremos hacer, no es capaz de detectar la cadena y
		 * se produce el fallo.
		 */
		
		/*
		 * Una posible solucion seria solicitar al usuario en el mensaje inicial 
		 * que incluya una cadena con el determinado caracter que queremos aislar. 
		 * Ejemplo: escribe una cadena de caracteres que incluya la letra 'a'.
		 */
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
		
	}

}