package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		/* El programa solicita un numero y, a traves de un bucle, lo divide entre todos
		 * los numeros existentes entre el 1 y ese mismo numero. Al bucle se incorpora
		 * tambien un contador de divisores, de manera que si la division entre el numero introducido y el 
		 * divisor es cero, aumenta en uno ese contador. De esta forma, al final del
		 * bucle, sabremos el numero de divisores que tiene la cifra introducida. 
		 * 
		 * El programa, ademas, consta de una segunda parte: si en el contador aparecen mas de dos 
		 * divisores, se muestra el mensaje "No lo es", en referencia a que el numero no es primo. Por 
		 * el contrario, si tiene solo dos o un divisor, se muestra el mensaje "Si lo es", 
		 * en referencia a que es un numero primo o que solo se puede dividir por el numero uno 
		 * (esto ultimo solo se da en el caso del numero uno). 
		 * 
		 */
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
