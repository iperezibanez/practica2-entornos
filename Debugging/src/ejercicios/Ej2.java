package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		/*
		 * Cuando ingresamos un numero, aparte de esa cifra, en el buffer queda almacenado el 
		 * caracter �\n� (enter). Esto produce que, cuando en la l�nea siguiente queremos
		 * ingresar una cadena de texto, el primer caracter que lee el programa es el enter arrastrado,
		 * por lo que no se guarda nada. 
		 * 
		 */
		
		/*Soluci�n: hay que escribir despu�s del cadenaLeida = input.nextLine(); un 'input.nextLine();'
		 * 
		 */
		
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}