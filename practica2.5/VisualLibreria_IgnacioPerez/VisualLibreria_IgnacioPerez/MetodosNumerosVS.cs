﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualLibreria_IgnacioPerez
{
    public class MetodosNumerosVS
    {

        public static void esPrimo(int numeroIntroducido)
        {

            int contadorDivisores = 0;

            for (int i = 1; i <= numeroIntroducido; i++)
            {
                if (numeroIntroducido % i == 0)
                {
                    contadorDivisores++;
                }
            }

            if (contadorDivisores != 2)
            {
                Console.WriteLine("No es primo.");
            }
            else
            {
                Console.WriteLine("Es primo.");
            }

        }

        public static void esPerfecto(int numeroIntroducido)
        {

            int sumaTotal = 0;

            for (int i = 1; i < numeroIntroducido; i++)
            {
                if (numeroIntroducido % i == 0)
                {
                    sumaTotal = sumaTotal + i;
                }
            }

            if (sumaTotal != numeroIntroducido)
            {
                Console.WriteLine("No es perfecto.");
            }
            else
            {
                Console.WriteLine("Es perfecto.");
            }

        }

        public static void tablaMultiplicar(int numeroIntroducido)
        {

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine(numeroIntroducido + " x " + i + " = " + (numeroIntroducido * i));

            }

        }

        public static void cantidadPrimos(int numeroIntroducido)
        {

            int contadorDivisores;
            int contadorPrimos = 0;

            for (int i = 1; i <= numeroIntroducido; i++)
            {

                contadorDivisores = 0;
                for (int j = 1; j <= i; j++)
                {

                    if (i % j == 0)
                    {
                        contadorDivisores++;
                    }
                }

                if (contadorDivisores == 2)
                {
                    contadorPrimos++;
                }
            }

            Console.WriteLine("La cantidad de primos entre 1 y " + numeroIntroducido + " es: " + contadorPrimos);

        }

        public static void factorial(int numeroIntroducido)
        {

            double total = 1;

            for (int i = 1; i <= numeroIntroducido; i++)
            {
                total *= i;
            }

            Console.WriteLine("Su factorial es " + total);
        }
    }
}
