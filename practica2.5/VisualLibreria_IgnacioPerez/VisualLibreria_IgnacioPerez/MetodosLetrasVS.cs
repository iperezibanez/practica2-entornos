﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisualLibreria_IgnacioPerez
{
    public class MetodosLetrasVS
    {

        public static String invertirCadena(String cadenaIntroducida)
        {

            String cadenaInversa = "";

            for (int i = 0; i < cadenaIntroducida.Length; i++)
            {
                cadenaInversa = cadenaIntroducida[i] + cadenaInversa;
            }

            return cadenaInversa;
        }

        public static void hayMayusculas(String cadenaIntroducida)
        {

            int contador = 0;

            for (int i = 0; i < cadenaIntroducida.Length; i++)
            {

                if (cadenaIntroducida[i] >= 65 && cadenaIntroducida[i] <= 90)
                {

                    contador++;

                }

            }

            if (contador >= 1)
            {

                Console.WriteLine("Tiene mayúsculas.");

            }
            else
            {

                Console.WriteLine("No tiene mayúsculas.");

            }

        }

        public static void desplazamiento(String cadenaIntroducida, int numeroDesplazamiento)
        {

            String nuevaPalabra = "";

            for (int i = numeroDesplazamiento; i < cadenaIntroducida.Length; i++)
            {

                nuevaPalabra = nuevaPalabra + cadenaIntroducida[i];

            }

            for (int j = 0; j < numeroDesplazamiento; j++)
            {

                nuevaPalabra = nuevaPalabra + cadenaIntroducida[j];

            }

            Console.WriteLine(nuevaPalabra);

        }

        public static void porcentajeVocales(String cadenaIntroducida)
        {

            float numeroCaracteres = cadenaIntroducida.Length;

            float contadorVocales = 0;

            for (int i = 0; i < cadenaIntroducida.Length; i++)
            {

                if (cadenaIntroducida[i] == 'a' || cadenaIntroducida[i] == 'e'
                        || cadenaIntroducida[i] == 'i' || cadenaIntroducida[i] == 'o'
                        || cadenaIntroducida[i] == 'u')
                {

                    contadorVocales++;

                }

            }

            Console.WriteLine("El porcentaje de vocales es " + (float)(contadorVocales / numeroCaracteres) * 100 + "%");

        }

        public static void cambiarLetras(String cadenaIntroducida, char letraEscogida, char letraCambio)
        {

            cadenaIntroducida = cadenaIntroducida.Replace(letraEscogida, letraCambio);

            Console.WriteLine(cadenaIntroducida);

        }
    }
}
