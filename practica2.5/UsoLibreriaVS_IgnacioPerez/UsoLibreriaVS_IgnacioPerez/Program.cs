﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisualLibreria_IgnacioPerez;


namespace UsoLibreriaVS_IgnacioPerez
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Introduzca el número deseado: ");
            int numeroIntroducido = int.Parse(Console.ReadLine());

            MetodosNumerosVS.cantidadPrimos(numeroIntroducido);
            MetodosNumerosVS.esPerfecto(numeroIntroducido);
            MetodosNumerosVS.esPrimo(numeroIntroducido);
            MetodosNumerosVS.factorial(numeroIntroducido);
            MetodosNumerosVS.tablaMultiplicar(numeroIntroducido);
            Console.WriteLine();

            Console.WriteLine("Introduzca la palabra deseada: ");

            String cadenaIntroducida = Console.ReadLine();

            MetodosLetrasVS.hayMayusculas(cadenaIntroducida);

            Console.WriteLine("Su inversión es " + MetodosLetrasVS.invertirCadena(cadenaIntroducida));

            MetodosLetrasVS.porcentajeVocales(cadenaIntroducida);

            Console.WriteLine("Introduzca cuántas posiciones quiere desplazar la palabra: ");
            int numeroDesplazamiento = int.Parse(Console.ReadLine());
            MetodosLetrasVS.desplazamiento(cadenaIntroducida, numeroDesplazamiento);

            Console.WriteLine("Introduzca la letra que quiere cambiar: ");
            char letraEscogida = Console.ReadLine()[0];
            Console.WriteLine("Introduzca la letra que quiere que aparezca en su lugar: ");
            char letraCambio = Console.ReadLine()[0];
            MetodosLetrasVS.cambiarLetras(cadenaIntroducida, letraEscogida, letraCambio);

        }
    }
}
