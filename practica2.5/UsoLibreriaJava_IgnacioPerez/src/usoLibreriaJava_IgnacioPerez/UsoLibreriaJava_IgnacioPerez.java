package usoLibreriaJava_IgnacioPerez;

import java.util.Scanner;

public class UsoLibreriaJava_IgnacioPerez {

	public static void main(String[] args) {

		Scanner lector = new Scanner(System.in);

		System.out.println("Introduzca el n�mero deseado: ");

		int numeroIntroducido = lector.nextInt();

		JavaLibreria_IgnacioPerez.MetodosNumeros.cantidadPrimos(numeroIntroducido);
		System.out.println();
		JavaLibreria_IgnacioPerez.MetodosNumeros.esPerfecto(numeroIntroducido);
		System.out.println();
		JavaLibreria_IgnacioPerez.MetodosNumeros.esPrimo(numeroIntroducido);
		System.out.println();
		JavaLibreria_IgnacioPerez.MetodosNumeros.factorial(numeroIntroducido);
		System.out.println();
		JavaLibreria_IgnacioPerez.MetodosNumeros.tablaMultiplicar(numeroIntroducido);
		System.out.println();

		lector.nextLine();

		System.out.println("Introduzca la palabra deseada: ");

		String cadenaIntroducida = lector.nextLine();

		JavaLibreria_IgnacioPerez.MetodosLetras.hayMayusculas(cadenaIntroducida);
		System.out.println();
		System.out.println(
				"Su inversi�n es " + JavaLibreria_IgnacioPerez.MetodosLetras.invertirCadena(cadenaIntroducida));
		System.out.println();
		JavaLibreria_IgnacioPerez.MetodosLetras.porcentajeVocales(cadenaIntroducida);
		System.out.println();
		System.out.println("Introduzca cu�ntas posiciones quiere desplazar la palabra: ");
		int numeroDesplazamiento = lector.nextInt();
		JavaLibreria_IgnacioPerez.MetodosLetras.desplazamiento(cadenaIntroducida, numeroDesplazamiento);
		System.out.println();
		lector.nextLine();
		System.out.println("Introduzca la letra que quiere cambiar: ");
		char letraEscogida = lector.nextLine().charAt(0);
		System.out.println("Introduzca la letra que quiere que aparezca en su lugar: ");
		char letraCambio = lector.nextLine().charAt(0);
		JavaLibreria_IgnacioPerez.MetodosLetras.cambiarLetras(cadenaIntroducida, letraEscogida, letraCambio);

		lector.close();

	}

}
