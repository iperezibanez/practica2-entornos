package JavaLibreria_IgnacioPerez;

public class MetodosNumeros {

	public static void esPrimo(int numeroIntroducido) {

		int contadorDivisores = 0;

		for (int i = 1; i <= numeroIntroducido; i++) {
			if (numeroIntroducido % i == 0) {
				contadorDivisores++;
			}
		}

		if (contadorDivisores != 2) {
			System.out.println("No es primo.");
		} else {
			System.out.println("Es primo.");
		}

	}

	public static void esPerfecto(int numeroIntroducido) {

		int sumaTotal = 0;

		for (int i = 1; i < numeroIntroducido; i++) {
			if (numeroIntroducido % i == 0) {
				sumaTotal = sumaTotal + i;
			}
		}

		if (sumaTotal != numeroIntroducido) {
			System.out.println("No es perfecto.");
		} else {
			System.out.println("Es perfecto.");
		}

	}

	public static void tablaMultiplicar(int numeroIntroducido) {

		for (int i = 1; i <= 10; i++) {
			System.out.println(numeroIntroducido + " x " + i + " = " + (numeroIntroducido * i));

		}

	}

	public static void cantidadPrimos(int numeroIntroducido) {

		int contadorDivisores;
		int contadorPrimos = 0;

		for (int i = 1; i <= numeroIntroducido; i++) {

			contadorDivisores = 0;
			for (int j = 1; j <= i; j++) {

				if (i % j == 0) {
					contadorDivisores++;
				}
			}

			if (contadorDivisores == 2) {
				contadorPrimos++;
			}
		}

		System.out.println("La cantidad de primos entre 1 y " + numeroIntroducido + " es: " + contadorPrimos);

	}

	public static void factorial(int numeroIntroducido) {

		double total = 1;

		for (int i = 1; i <= numeroIntroducido; i++) {
			total *= i;
		}

		System.out.println("Su factorial es " + total);
	}

}
