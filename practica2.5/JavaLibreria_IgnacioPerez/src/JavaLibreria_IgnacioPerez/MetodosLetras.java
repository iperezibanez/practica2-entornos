package JavaLibreria_IgnacioPerez;

public class MetodosLetras {

	public static String invertirCadena(String cadenaIntroducida) {

		String cadenaInversa = "";

		for (int i = 0; i < cadenaIntroducida.length(); i++) {
			cadenaInversa = cadenaIntroducida.charAt(i) + cadenaInversa;
		}

		return cadenaInversa;
	}

	public static void hayMayusculas(String cadenaIntroducida) {

		int contador = 0;

		for (int i = 0; i < cadenaIntroducida.length(); i++) {

			if (cadenaIntroducida.charAt(i) >= 65 && cadenaIntroducida.charAt(i) <= 90) {

				contador++;

			}

		}

		if (contador >= 1) {

			System.out.println("Tiene may�sculas.");

		} else {

			System.out.println("No tiene may�sculas.");

		}

	}

	public static void desplazamiento(String cadenaIntroducida, int numeroDesplazamiento) {

		String nuevaPalabra = "";

		for (int i = numeroDesplazamiento; i < cadenaIntroducida.length(); i++) {

			nuevaPalabra = nuevaPalabra + cadenaIntroducida.charAt(i);

		}

		for (int j = 0; j < numeroDesplazamiento; j++) {

			nuevaPalabra = nuevaPalabra + cadenaIntroducida.charAt(j);

		}

		System.out.println(nuevaPalabra);

	}

	public static void porcentajeVocales(String cadenaIntroducida) {

		float numeroCaracteres = cadenaIntroducida.length();

		float contadorVocales = 0;

		for (int i = 0; i < cadenaIntroducida.length(); i++) {

			if (cadenaIntroducida.charAt(i) == 'a' || cadenaIntroducida.charAt(i) == 'e'
					|| cadenaIntroducida.charAt(i) == 'i' || cadenaIntroducida.charAt(i) == 'o'
					|| cadenaIntroducida.charAt(i) == 'u') {

				contadorVocales++;

			}

		}

		System.out.println("El porcentaje de vocales es " + (float) (contadorVocales / numeroCaracteres) * 100 + "%");

	}

	public static void cambiarLetras(String cadenaIntroducida, char letraEscogida, char letraCambio) {

		cadenaIntroducida = cadenaIntroducida.replace(letraEscogida, letraCambio);

		System.out.println(cadenaIntroducida);

	}

}
