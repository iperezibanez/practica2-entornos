package proyecto_consola_Eclipse;

import java.util.Scanner;

public class proyecto_Eclipse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner lector = new Scanner(System.in);

		int eleccion;

		do {

			System.out.println("Menu");
			System.out.println("1 - Hay letras");
			System.out.println("2 - Total de divisores");
			System.out.println("3 - Nombre y apellidos");
			System.out.println("4 - Factorial");
			System.out.println("5 - Salir del programa");

			System.out.println();
			System.out.println("Selecciona una opci�n num�rica: ");
			eleccion = lector.nextInt();
			lector.nextLine();

			switch (eleccion) {
			case 1:

				System.out.println("Introduce un string: ");
				String StringIntroducido = lector.nextLine();

				HayLetras(StringIntroducido);
				System.out.println();

				break;
			case 2:

				System.out.println("Introduce un n�mero");

				int numeroIntroducido = lector.nextInt();

				TotalDivisores(numeroIntroducido);
				System.out.println();

				break;

			case 3:

				System.out.println("Introduzca un apellido y un nombre en el formato 'apellido, nombre'");
				String identidad = lector.nextLine();

				nombres(identidad);
				System.out.println();

				break;

			case 4:

				System.out.println("Introduce un n�mero:");
				long numeroIntro = lector.nextLong();

				factores(numeroIntro);
				System.out.println();

				break;

			}
		} while (eleccion != 5);

		lector.close();

	}

	static void HayLetras(String StringIntroducido) {

		char letra;

		int contadorLetras = 0;

		for (int posicion = 0; posicion < StringIntroducido.length(); posicion++) {
			letra = StringIntroducido.charAt(posicion);

			if (letra >= 97 && letra <= 122) {

				contadorLetras = 1 + contadorLetras;

			} else {
			}

		}

		if (contadorLetras > 0) {

			System.out.println("Hay letras.");

		} else {

			System.out.println("No hay letras.");

		}
	}

	static void TotalDivisores(int numeroIntroducido) {

		int sumaDivisores = 0;

		for (int divisor = 1; divisor <= numeroIntroducido; divisor++) {

			if (numeroIntroducido % divisor == 0) {

				sumaDivisores = 1 + sumaDivisores;

			}

		}

		System.out.println("Tiene " + sumaDivisores + " divisores");

	}

	static void nombres(String identidad) {

		int posicionComa = identidad.indexOf(',');

		String apellido = identidad.substring(0, posicionComa);
		String nombre = identidad.substring(posicionComa+2);

		System.out.println("El nombre de la personas es " + nombre);
		System.out.println("El apellido de la persona es " + apellido);

		int sumatorio = 0;

		for (int posicion = 0; posicion < nombre.length(); posicion++) {

			if (nombre.charAt(posicion) >= 65 && nombre.charAt(posicion) <= 90) {

				sumatorio = sumatorio + 1;

			}

		}

		if (sumatorio >= 1) {

			System.out.println("El nombre tiene may�sculas");

		} else {
			System.out.println("El nombre no tiene may�sculas");

		}

	}

	static void factores(long numeroIntro) {

		long resultadoFactorial = 1;

		for (long componentes = 1; componentes <= numeroIntro; componentes++) {

			resultadoFactorial = componentes * resultadoFactorial;

		}

		System.out.println("El n�mero factorial es " + resultadoFactorial);

	}

}
