﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            int eleccion;

            do
            {

                Console.WriteLine("Menu");
                Console.WriteLine("1 - Hay letras");
                Console.WriteLine("2 - Total de divisores");
                Console.WriteLine("3 - Nombre y apellidos");
                Console.WriteLine("4 - Factorial");
                Console.WriteLine("5 - Salir del programa");

                Console.WriteLine();
                Console.WriteLine("Selecciona una opción numérica: ");
                eleccion = int.Parse(Console.ReadLine());

                switch (eleccion)
                {
                    case 1:

                        Console.WriteLine("Introduce un string: ");
                        string StringIntroducido = Console.ReadLine();

                        HayLetras(StringIntroducido);
                        Console.WriteLine();


                        break;
                    case 2:

                        Console.WriteLine("Introduce un número");

                        int numeroIntroducido = int.Parse(Console.ReadLine());

                        TotalDivisores(numeroIntroducido);
                        Console.WriteLine();

                        break;

                    case 3:

                        Console.WriteLine("Introduzca un apellido y un nombre en el formato 'apellido, nombre'");
                        String identidad = Console.ReadLine();

                        nombres(identidad);
                        Console.WriteLine();

                        break;

                    case 4:


                        Console.WriteLine("Introduce un número:");
                        long numeroIntro = long.Parse(Console.ReadLine());

                        factores(numeroIntro);
                        Console.WriteLine();

                        break;

                }
            } while (eleccion != 5);


        }

        static void HayLetras(string StringIntroducido)
        {

            char letra;

            int contadorLetras = 0;

            for (int posicion = 0; posicion < StringIntroducido.Length; posicion++)
            {
                letra = StringIntroducido[posicion];


                if (letra >= 97 && letra <= 122)
                {

                    contadorLetras = 1 + contadorLetras;

                }
                else
                {
                }

            }

            if (contadorLetras > 0)
            {

                Console.WriteLine("Hay letras.");

            }
            else
            {

                Console.WriteLine("No hay letras.");

            }
        }

        static void TotalDivisores(int numeroIntroducido)
        {

            int sumaDivisores = 0;

            for (int divisor = 1; divisor <= numeroIntroducido; divisor++)
            {

                if (numeroIntroducido % divisor == 0)
                {

                    sumaDivisores = 1 + sumaDivisores;

                }

            }

            Console.WriteLine("Tiene {0} divisores", sumaDivisores);

        }

        static void nombres(String identidad)
        {

            int posicionComa = identidad.IndexOf(',');

            String apellido = identidad.Substring(0, posicionComa);
            String nombre = identidad.Substring(posicionComa + 2);

            Console.WriteLine("El nombre de la persona es {0}", nombre);
            Console.WriteLine("El apellido de la persona es {0}", apellido);

            int sumatorio = 0;

            for (int posicion = 0; posicion < nombre.Length; posicion++)
            {

                if (nombre[posicion] >= 65 && nombre[posicion] <= 90)
                {

                    sumatorio = sumatorio + 1;

                }

            }

            if (sumatorio >= 1)
            {

                Console.WriteLine("El nombre tiene mayúsculas");

            }
            else
            {
                Console.WriteLine("El nombre no tiene mayúsculas");

            }


        }

        static void factores(long numeroIntro)
        {

            long resultadoFactorial = 1;

            for (long componentes = 1; componentes <= numeroIntro; componentes++)
            {

                resultadoFactorial = componentes * resultadoFactorial;

            }

            Console.WriteLine("El número factorial es {0}", resultadoFactorial);


        }

    }
}

